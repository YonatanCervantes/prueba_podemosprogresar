--CREATE USER 'eval'@'localhost' IDENTIFIED BY 'password';
--GRANT ALL PRIVILEGES ON podemos_eval.* TO 'eval'@'localhost' IDENTIFIED BY 'password';

CREATE USER eval WITH PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE podemos_progresar TO eval;
