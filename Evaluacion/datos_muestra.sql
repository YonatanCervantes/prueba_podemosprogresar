-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 05, 2020 at 01:38 AM
-- Server version: 10.5.6-MariaDB
-- PHP Version: 7.4.12

--SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
--SET time_zone = 'UTC';
START TRANSACTION;



/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `podemos_eval`
--

--
-- Dumping data for table `Clientes`
--

--INSERT INTO `Clientes` (`id`, `nombre`) VALUES
--('ABCDE03', 'IRMA MARQUEZ RUBIO'),
--('ASDFG08', 'SANDRA SANCHEZ GONZALEZ'),
--('HJKLL09', 'ANGELA GOMEZ MONROY'),
--('MNOPQ01', 'GERTRUDIS LOPEZ MARTINEZ'),
--('NMZXC11', 'DANIELA HERNANDEZ GUERRERO'),
--('OPQRS04', 'ALEIDA SANCHEZ AMOR'),
--('QRSTU02', 'FERNANDA JUAREZ LOPEZ'),
--('QWERT06', 'ALBA PEREZ TORRES'),
--('TYUIQ05', 'LORENA GARCIA ROCHA'),
--('YUIOP07', 'ELISEO CHAVEZ OLVERA'),
--('ZXCVB10', 'KARLA ENRIQUEZ NAVARRETE');

INSERT INTO podemos_eval.prestamos_Clientes (id, nombre) VALUES
('ABCDE03', 'IRMA MARQUEZ RUBIO'),
('ASDFG08', 'SANDRA SANCHEZ GONZALEZ'),
('HJKLL09', 'ANGELA GOMEZ MONROY'),
('MNOPQ01', 'GERTRUDIS LOPEZ MARTINEZ'),
('NMZXC11', 'DANIELA HERNANDEZ GUERRERO'),
('OPQRS04', 'ALEIDA SANCHEZ AMOR'),
('QRSTU02', 'FERNANDA JUAREZ LOPEZ'),
('QWERT06', 'ALBA PEREZ TORRES'),
('TYUIQ05', 'LORENA GARCIA ROCHA'),
('YUIOP07', 'ELISEO CHAVEZ OLVERA'),
('ZXCVB10', 'KARLA ENRIQUEZ NAVARRETE');

--
-- Dumping data for table `Grupos`
--

--INSERT INTO `Grupos` (`id`, `nombre`) VALUES
--('ABCD2', 'CHARLIE\'S ANGELS'),
--('GHIJK', 'KITTIE'),
--('XYZW1', 'POWERPUFF GIRLS');

INSERT INTO podemos_eval.prestamos_Grupos (id, nombre) VALUES
('ABCD2', 'CHARLIES ANGELS'),
('GHIJK', 'KITTIE'),
('XYZW1', 'POWERPUFF GIRLS');

--
-- Dumping data for table `Miembros`
--

--INSERT INTO `Miembros` (`grupo_id`, `cliente_id`) VALUES
--('ABCD2', 'ASDFG08'),
--('ABCD2', 'QWERT06'),
--('ABCD2', 'TYUIQ05'),
--('ABCD2', 'YUIOP07'),
--('GHIJK', 'HJKLL09'),
--('GHIJK', 'NMZXC11'),
--('GHIJK', 'ZXCVB10'),
--('XYZW1', 'ABCDE03'),
--('XYZW1', 'MNOPQ01'),
--('XYZW1', 'OPQRS04'),
--('XYZW1', 'QRSTU02');

INSERT INTO podemos_eval.prestamos_Miembros (grupo_id, cliente_id) VALUES
('ABCD2', 'ASDFG08'),
('ABCD2', 'QWERT06'),
('ABCD2', 'TYUIQ05'),
('ABCD2', 'YUIOP07'),
('GHIJK', 'HJKLL09'),
('GHIJK', 'NMZXC11'),
('GHIJK', 'ZXCVB10'),
('XYZW1', 'ABCDE03'),
('XYZW1', 'MNOPQ01'),
('XYZW1', 'OPQRS04'),
('XYZW1', 'QRSTU02');

--
-- Dumping data for table `Cuentas`
--

--INSERT INTO `Cuentas` (`id`, `grupo_id`, `estatus`, `monto`, `saldo`) VALUES
--('10001', 'XYZW1', 'DESEMBOLSADA', '150000.00', '74500.00'),
--('12345', 'ABCD2', 'DESEMBOLSADA', '75000.00', '64500.00'),
--('23001', 'XYZW1', 'CERRADA', '60000.00', '0.00'),
--('89752', 'GHIJK', 'DESEMBOLSADA', '80000.00', '80000.00');

INSERT INTO podemos_eval.prestamos_Cuentas (id, grupo_id, estatus, monto, saldo) VALUES
('10001', 'XYZW1', 'DESEMBOLSADA', '150000.00', '74500.00'),
('12345', 'ABCD2', 'DESEMBOLSADA', '75000.00', '64500.00'),
('23001', 'XYZW1', 'CERRADA', '60000.00', '0.00'),
('89752', 'GHIJK', 'DESEMBOLSADA', '80000.00', '80000.00');

--
-- Dumping data for table `CalendarioPagos`
--

--INSERT INTO `CalendarioPagos` (`id`, `cuenta_id`, `num_pago`, `monto`, `fecha_pago`, `estatus`) VALUES
--(1, '23001', 1, '15000.00', '2018-11-30', 'PAGADO'),
--(2, '23001', 2, '15000.00', '2018-12-07', 'PAGADO'),
--(3, '23001', 3, '15000.00', '2018-12-14', 'PAGADO'),
--(4, '23001', 4, '15000.00', '2018-12-21', 'PAGADO'),
--(5, '12345', 1, '18750.00', '2018-12-20', 'PARCIAL'),
--(6, '12345', 2, '18750.00', '2018-12-27', 'PENDIENTE'),
--(7, '12345', 3, '18750.00', '2019-01-03', 'PENDIENTE'),
--(8, '12345', 4, '18750.00', '2019-01-10', 'PENDIENTE'),
--(9, '10001', 1, '37500.00', '2018-12-07', 'PAGADO'),
--(10, '10001', 2, '37500.00', '2018-12-14', 'PAGADO'),
--(11, '10001', 3, '37500.00', '2018-12-21', 'PARCIAL'),
--(12, '10001', 4, '37500.00', '2018-12-28', 'PENDIENTE'),
--(13, '89752', 1, '20000.00', '2019-01-07', 'ATRASADO'),
--(14, '89752', 2, '20000.00', '2019-01-14', 'PENDIENTE'),
--(15, '89752', 3, '20000.00', '2019-01-21', 'PENDIENTE'),
--(16, '89752', 4, '20000.00', '2019-01-28', 'PENDIENTE');

INSERT INTO podemos_eval.prestamos_CalendarioPagos (id, cuenta_id, num_pago, monto, fecha_pago, estatus) VALUES
(1, '23001', 1, '15000.00', '2018-11-30', 'PAGADO'),
(2, '23001', 2, '15000.00', '2018-12-07', 'PAGADO'),
(3, '23001', 3, '15000.00', '2018-12-14', 'PAGADO'),
(4, '23001', 4, '15000.00', '2018-12-21', 'PAGADO'),
(5, '12345', 1, '18750.00', '2018-12-20', 'PARCIAL'),
(6, '12345', 2, '18750.00', '2018-12-27', 'PENDIENTE'),
(7, '12345', 3, '18750.00', '2019-01-03', 'PENDIENTE'),
(8, '12345', 4, '18750.00', '2019-01-10', 'PENDIENTE'),
(9, '10001', 1, '37500.00', '2018-12-07', 'PAGADO'),
(10, '10001', 2, '37500.00', '2018-12-14', 'PAGADO'),
(11, '10001', 3, '37500.00', '2018-12-21', 'PARCIAL'),
(12, '10001', 4, '37500.00', '2018-12-28', 'PENDIENTE'),
(13, '89752', 1, '20000.00', '2019-01-07', 'ATRASADO'),
(14, '89752', 2, '20000.00', '2019-01-14', 'PENDIENTE'),
(15, '89752', 3, '20000.00', '2019-01-21', 'PENDIENTE'),
(16, '89752', 4, '20000.00', '2019-01-28', 'PENDIENTE');

--
-- Dumping data for table `Transacciones`
--

--INSERT INTO `Transacciones` (`id`, `cuenta_id`, `fecha`, `monto`) VALUES
--(1, '23001', '2030-11-18 10:36:00', '15000.00'),
--(2, '23001', '2007-12-18 12:50:00', '15000.00'),
--(3, '23001', '2014-12-18 13:45:00', '15000.00'),
--(4, '23001', '2021-12-18 11:35:00', '15000.00'),
--(5, '12345', '2020-12-18 14:50:00', '5000.00'),
--(6, '12345', '2021-12-18 15:25:00', '5500.00'),
--(7, '10001', '2007-12-18 11:34:00', '37500.00'),
--(8, '10001', '2007-12-18 10:04:00', '37500.00'),
--(9, '10001', '2007-12-18 18:50:00', '-30000.00'),
--(10, '10001', '2007-12-18 18:51:00', '-7500.00'),
--(11, '10001', '2014-12-18 09:59:00', '37500.00'),
--(12, '10001', '2021-12-18 11:05:00', '500.00');
--COMMIT;

INSERT INTO podemos_eval.prestamos_Transacciones (id, cuenta_id, fecha, monto) VALUES
(1, '23001', '2030-11-18 10:36:00', '15000.00'),
(2, '23001', '2007-12-18 12:50:00', '15000.00'),
(3, '23001', '2014-12-18 13:45:00', '15000.00'),
(4, '23001', '2021-12-18 11:35:00', '15000.00'),
(5, '12345', '2020-12-18 14:50:00', '5000.00'),
(6, '12345', '2021-12-18 15:25:00', '5500.00'),
(7, '10001', '2007-12-18 11:34:00', '37500.00'),
(8, '10001', '2007-12-18 10:04:00', '37500.00'),
(9, '10001', '2007-12-18 18:50:00', '-30000.00'),
(10, '10001', '2007-12-18 18:51:00', '-7500.00'),
(11, '10001', '2014-12-18 09:59:00', '37500.00'),
(12, '10001', '2021-12-18 11:05:00', '500.00');
COMMIT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
