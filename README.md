# Prueba Podemos progresar

Se implemento el sistema de acuerdo a los requerimientos proporcionados en el documento
En seguida se listan la serie de pasos para deployar correctamente el proyecto.

Requerimientos:

Python 3.6.9

PostgreSQL

Ubuntu 18.04

Tecnologías utilizadas:

	DjangoRestFramework

	HTML-JavaScript

Instrucciones, dentro de la carpeta Evaluacion (en el repositorio) se encuentran las instrucciones sobre las que se baso el desarrollo, ademas de los scripts de DB necesarios para las pruebas, cabe mencionar que estos scripts no son los originales ya que se tuvo que hacer una adaptacion de acuerdo a lo necesitado para deployar.
Despues de haber instalado postgres y la version de pyhton.

1- Abrir consola linux

2- sudo -i -u postgres (sudo pedira autenticacio)

3- psql

4- CREATE DATABASE podemos_progresar;

5- \q

6- Clonar el siguiente repositorio de git: git clone https://gitlab.com/YonatanCervantes/prueba_podemosprogresar

7- Obtener la ruta de los scripts y regresar a la consola psql para ejecutarlos

	psql podemos_progresar < ruta/admin.sql

	psql podemos_progresar < ruta/model_script.sql

	psql podemos_progresar < ruta/datos_muestra.sql

 En este punto ya tenemos nuestra DB poblada

8- Crear un Virtual Enviroment con: python3.6 -m venv NombreEV

9- Activar el EV con: source NombreEV/bin/activate

9- Mediante consola posiscionarnos dentro de la raiz del proyecto (/prueba_podemosprogresar), ahi encontraremos el archivo requirements.txt

9- Instalar dependencias con: pip install -r requirements.txt

10- python manage.py runserver

11- Dentro de la carpeta "front" encontrara el archivo "index.html", abrir este archivo con un navegador y liso, podra comenzar  interactuar con el ambiente de pruebas.


#Notas: Se desarrollaron los requerimientos establecidos en el documento, aexcepccion de los bonus y las pruebas unitarias, como bonus se anexo la parte de ingresar nuevos grupos, sin embargo el front no se incluyo.