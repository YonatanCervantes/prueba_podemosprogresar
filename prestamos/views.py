from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import viewsets, mixins
from rest_framework import status
from .serializers import *
from .models import *
import json

# Create your views here.
class PagoViewSetPost(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
        url:http://127.0.0.1:8000/prestamos/pago/
        metodo:POST
        body:
        {
            "cuenta": "12345",
            "cantidad": 45749
        }
    """
    serializer_class = PagoSerializerPost

    def create(self, request, *args, **kwargs):
        """
            Nuevo pago.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        response = "Pago registrado exitosamente!"
        return Response(response)


pago_post = PagoViewSetPost.as_view(
    name="Pago",
    description="Registrar pago",
    actions={
        'post': 'create'
    }
)

class GrupoViewSetGet(viewsets.ModelViewSet):
    """
        url:http://127.0.0.1:8000/prestamos/grupo_ver/ABCD2
        metodo:GET
    """
    serializer_class = GrupoSerializerGet

    def list(self, request,  *args, **kwargs):
        """
            Retorna la informacion de un grupo (datos, transacciones y calendario)
        """
        instance = self.kwargs["pk"]
        serializer = self.get_serializer(instance,data=request.data)
        serializer.is_valid(raise_exception=True)
        response = serializer.list(instance, serializer.validated_data)
        return Response(response)
        #q = self.get_queryset().values('id','nombre')
        #return Response(list(q))


grupo_get = GrupoViewSetGet.as_view(
    name="Grupo",
    description="Listar calendario y transacciones de un grupo",
    actions={
        'get': 'list'
    }
)

class GruposViewSetGet(viewsets.ModelViewSet):
    """
        url:http://127.0.0.1:8000/prestamos/grupos_ver/
        metodo:GET
    """
    serializer_class = GruposSerializerGet

    def list(self, request):
        """
            Retorna todos los grupos existentes junto con sus miembros
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = serializer.list(serializer.validated_data)
        return Response(response)

grupos_get = GruposViewSetGet.as_view(
    name="Grupos",
    description="Listar grupos",
    actions={
        'get': 'list'
    }
)

class ClientesViewSetGet(viewsets.ModelViewSet):
    """
        url:http://127.0.0.1:8000/prestamos/clientes_ver/
        metodo:GET
    """
    queryset = Clientes.objects.all()

    def list(self, request):
        """
            Retorna todos los clientes existentes en la base de datos
        """
        q = self.get_queryset().values('id','nombre')
        return Response(list(q))


clientes_get = ClientesViewSetGet.as_view(
    name="Clientes",
    description="Listar clientes",
    actions={
        'get': 'list'
    }
)

class GruposViewSetPost(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
        url:http://127.0.0.1:8000/prestamos/grupos/
        metodo:POST
        body:
        {
            "nombre": "grupo1",
            "clientes": [
                "ABCDE03",
                "ASDFG08",
                "ABCDE03"
            ]
        }
    """
    serializer_class = GruposSerializerPost

    def create(self, request, *args, **kwargs):
        """
            Crear un nuevo grupo
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        response = "Grupo creado exitosamente!"
        return Response(response)


grupo_post = GruposViewSetPost.as_view(
    name="Grupo",
    description="Crear grupos",
    actions={
        'post': 'create'
    }
)



