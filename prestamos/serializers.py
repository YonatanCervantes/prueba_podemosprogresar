from rest_framework import serializers
from .models import *
from rest_framework.response import Response
import random
from django.utils import timezone
import datetime

class PagoSerializerPost(serializers.Serializer):
    cuenta = serializers.CharField(label="Id de la cuenta",
                                required=True)

    cantidad = serializers.DecimalField(label="Cantidad del pago",
                                max_digits=15, 
                                decimal_places=2,
                                required=True)

    def validate_cuenta(self, value):
        """
            Valida que la cuenta exista en la base de datos, en caso de existir, valida que no este cerrada.
        """
        if not Cuentas.objects.filter(id__iexact=value).exists():
            message = "La cuenta: '" + value + "' no existe en la base de datos."
            raise serializers.ValidationError(message)
        else:
            objCuenta=Cuentas.objects.get(id__iexact=value)
            if objCuenta.estatus == 'CERRADA':
                message = "La cuenta: '" + value + "' ya esta saldada, no es necesario hacer mas pagos."
                raise serializers.ValidationError(message)
        return value

    def validate_cantidad(self, value):
        """
            Valida que la cantidad introducida no sea negativa ni igual a 0.
        """
        if value <= 0:
            message = "El campo cantidad no puede ser menor o igual que 0"
            raise serializers.ValidationError(message)
        return value

    def validate(self, validated_data):
        '''
            Valida que la cantidad introducida no sea mayor a la restante de la cuenta
        '''
        id_cuenta = validated_data['cuenta']
        cantidad = validated_data['cantidad']
        objCuenta = Cuentas.objects.get(id__iexact=id_cuenta)
        if objCuenta.saldo < cantidad:
            message = "El campo cantidad no puede ser mayor a la cantidad de saldo restante de la cuenta."
            raise serializers.ValidationError(message)
        validated_data['objCuenta']=objCuenta
        return validated_data

    def create(self, validated_data):
        '''
            Registra la cantidad asignada en la cuenta, modificando los parametros de los pagos que alcance a cubrir dicha
            cantidad, ademas de cambiar los valores de estatus de la cuenta de acuerdo al monto saldado y el estatus de los
            pagos de acuerdo a la fecha en que se reliza la transacción, por ultimo registra esta transacción con sus parametros.
        '''
        cuenta = validated_data['objCuenta']
        cantidad_i = validated_data['cantidad']
        calendario = CalendarioPagos.objects.filter(cuenta_id=cuenta).order_by('num_pago')
        cantidad = cantidad_i+(cuenta.monto-cuenta.saldo)#cantidad total de la transaccion actual mas transacciones anteriores
        for pago in calendario:
            if pago.estatus == 'PAGADO':
                cantidad = cantidad-pago.monto
            elif cantidad < pago.monto:
                pago.estatus = 'PARCIAL'
                pago.save()
                break
            elif cantidad == pago.monto:
                pago.estatus = 'PAGADO'
                pago.save()
                break
            elif cantidad > pago.monto:
                cantidad = cantidad-pago.monto
                pago.estatus = 'PAGADO'
                pago.save()
        cuenta.saldo = cuenta.saldo-cantidad_i
        fecha_hoy = datetime.datetime.now(tz=timezone.utc)
        for pago in calendario:
            if pago.estatus == 'PENDIENTE' or pago.estatus == 'PARCIAL':
                if pago.fecha_pago < fecha_hoy.date():
                    pago.estatus = 'ATRASADO'
                    pago.save()
        if cuenta.saldo == 0:
            cuenta.estatus = 'CERRADA'
        cuenta.save()
        numeroTransacciones = Transacciones.objects.all().count()
        newTransaccion = Transacciones(id=numeroTransacciones+1, cuenta_id=cuenta, fecha=fecha_hoy, monto=cantidad_i)
        newTransaccion.save()
        return validated_data

class GrupoSerializerGet(serializers.Serializer):

    def validate(self, validated_data):
        """
            Valida que el id del grupo solicitado exista
        """
        id_grupo = self.context['request'].parser_context['kwargs']['pk']
        if Grupos.objects.filter(pk=id_grupo).exists():
            objGrupo = Grupos.objects.get(pk=id_grupo)
            validated_data['grupo'] = objGrupo
        else:
            message = "No existe un grupo con ID " + str(id_grupo)
            raise serializers.ValidationError(message)
        return validated_data

    def list(self, instance, validated_data):
        '''
            Retorna la consulta de un grupo, desglosando su calendario y sus transacciones.
        '''
        grupo = validated_data['grupo']
        result = []
        cuentas = Cuentas.objects.filter(grupo_id=grupo)
        for cuenta in cuentas:
            calendarios = CalendarioPagos.objects.filter(cuenta_id = cuenta)
            list_calendarios = []
            for calendario in calendarios:
                dic_calendario = {
                'num_pago':calendario.num_pago,
                'monto':calendario.monto,
                'fecha_pago':calendario.fecha_pago,
                'estatus':calendario.estatus
                }
                list_calendarios.append(dic_calendario)
            transacciones = Transacciones.objects.filter(cuenta_id= cuenta)
            list_transacciones = []
            for transaccion in transacciones:
                dic_transaccion = {
                'fecha':transaccion.fecha,
                'monto':transaccion.monto
                }
                list_transacciones.append(dic_transaccion)
            dic_cuenta = {
            'id':cuenta.id,
            'estatus':cuenta.estatus,
            'monto':cuenta.monto,
            'saldo':cuenta.saldo,
            'calendario':list_calendarios,
            'transacciones':list_transacciones
            }
            result.append(dic_cuenta)

        return result

class GruposSerializerGet(serializers.Serializer):

    def list(self, validated_data):
        '''
            Retorna la consulta de todos los grupos junto con sus miembros
        '''
        grupos = Grupos.objects.all()
        result = []
        for grupo in grupos:
            miembros_list = []
            objMiembros = Miembros.objects.filter(grupo_id=grupo)
            for miembro in objMiembros:
                miembros_list.append(miembro.cliente_id.nombre)
            grupo_miembros={
            'id' : grupo.id,
            'grupo' : grupo.nombre,
            'miembros' :  miembros_list
            }
            result.append(grupo_miembros)
        return result

class GruposSerializerPost(serializers.Serializer):
    nombre = serializers.CharField(label="Nombre del grupo",
                                 required=True)

    clientes = serializers.ListField(child=serializers.CharField(),
                                 label="Lista de ids de clientes",
                                 required=True)

    def validate_nombre(self, value):
        """
            Valida que el nombre sea unico en la base de datos
        """
        if Grupos.objects.filter(nombre__iexact=value).exists():
            message = "El nombre del grupo: '" + value + "' ya existe en la base de datos"
            raise serializers.ValidationError(message)
        return value

    def validate_clientes(self, value):
        """
            Se valida que los elemento dentro del arreglo de clientes, existan, no este vacío, contenga mas de un elemento
            y no este repetido en el grupo.
        """
        if len(value) == 0:
            message = "El campo clientes no puede estar vacio"
            raise serializers.ValidationError(message)
        elif len(value) == 1:
            message = "El campo clientes debe de tener al menos 2 elementos"
            raise serializers.ValidationError(message)
        for cliente in value:
            if value.count(cliente)>1:
                message = "El cliente con id " +  str(cliente) + " esta repetido en el grupo."
                raise serializers.ValidationError(message)
            if not Clientes.objects.filter(pk=cliente).exists():
                message = "El cliente con id " +  str(cliente) + " no existe."
                raise serializers.ValidationError(message)
        return value


    def create(self, validated_data):
        '''
                Crea un grupo nuevo, validando las condiciones para un nuevo grupo.
        '''
        nombre = validated_data['nombre']
        clientes = validated_data['clientes']
        MAYUS = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789'
        keylist = [random.choice(MAYUS) for i in range(5)]
        id_grupo = "".join(keylist)
        try:
            newGrupo = Grupos(id=id_grupo, nombre=nombre)
            newGrupo.save()
            for cliente in clientes:
                objCliente = Clientes.objects.get(pk=cliente)
                newMiembro = Miembros(grupo_id = newGrupo, cliente_id = objCliente)
                newMiembro.save()
            return validated_data
        except:
            message = "Hubo conflicto con la DB"
            raise serializers.ValidationError(message)