from django.conf.urls import url
from prestamos import views


urlpatterns = [
    url(r'^clientes_ver/$', views.clientes_get),
    url(r'^grupos_ver/$', views.grupos_get),
    url(r'^grupo_ver/(?P<pk>[A-Z0-9]+)$', views.grupo_get),
    url(r'^pago/$', views.pago_post),
    url(r'^grupos/$', views.grupo_post),
]