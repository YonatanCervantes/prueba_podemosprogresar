from django.db import models

# Create your models here.
class Clientes(models.Model):
    id = models.CharField("Id cliente",
                            max_length=7,
                            primary_key=True,
                            unique=True,
                            null=False)
    nombre = models.CharField("Nombre cliente",
                            max_length=60,
                            unique=False,
                            blank=False,
                            help_text="Cadena unica de a lo mas 60 caracteres",
                            null=False)
    def __str__(self):
        return str(self.nombre)

class Grupos(models.Model):
    id = models.CharField("Id grupo",
                            max_length=5,
                            primary_key=True,
                            unique=True,
                            null=False)
    nombre = models.CharField("Nombre grupo",
                            max_length=20,
                            unique=False,
                            blank=False,
                            help_text="Cadena unica de a lo mas 20 caracteres",
                            null=False)
    def __str__(self):
        return str(self.nombre)

class Miembros(models.Model):
    grupo_id = models.ForeignKey(Grupos,
                               db_column='grupo_id',
                               on_delete=models.DO_NOTHING,
                               blank=False,
                               null=False)
    cliente_id = models.ForeignKey(Clientes,
                                  db_column='cliente_id',
                                  on_delete=models.DO_NOTHING,
                                  blank=False,
                                  null=False)
    class Meta:
        unique_together = (("grupo_id", "cliente_id"),)

    def __str__(self):
        return str(self.grupo_id.nombre + ": " + self.cliente_id.nombre)

class Cuentas(models.Model):
    id = models.CharField("Id cuenta",
                            max_length=5,
                            primary_key=True,
                            unique=True,
                            null=False)
    grupo_id = models.ForeignKey(Grupos,
                                  db_column='grupo_id',
                                  on_delete=models.DO_NOTHING,
                                  blank=False,
                                  null=False)
    estatus = models.CharField("Estatus de la cuenta",
                            max_length=15,
                            unique=False,
                            blank=False,
                            help_text="Cadena de a lo mas 15 caracteres",
                            null=False)
    monto = models.DecimalField("Monto de la cuenta",
                            max_digits=15, 
                            decimal_places=2,
                            unique=False,
                            blank=False,
                            help_text="Decimal de a lo mas 15 digitos y 2 posiciones",
                            null=False)
    saldo = models.DecimalField("Saldo de la cuenta",
                            max_digits=15, 
                            decimal_places=2,
                            unique=False,
                            blank=False,
                            help_text="Decimal de a lo mas 15 digitos y 2 posiciones",
                            null=False)
    def __str__(self):
        return str(self.id + self.grupo_id.nombre + ": " + str(self.monto) + "/" + str(self.saldo) + "(" + self.estatus + ")")

class CalendarioPagos(models.Model):
    id = models.IntegerField("Id CalendarioPagos",
                            primary_key=True,
                            unique=True,
                            null=False)
    cuenta_id = models.ForeignKey(Cuentas,
                                  db_column='cuenta_id',
                                  on_delete=models.DO_NOTHING,
                                  blank=False,
                                  null=False)
    num_pago = models.IntegerField("Numero del pago",
                                    blank=True,
                                    default=0)
    monto = models.DecimalField("Monto de la cuenta",
                            max_digits=15, 
                            decimal_places=2,
                            unique=False,
                            blank=False,
                            help_text="Decimal de a lo mas 15 digitos y 2 posiciones",
                            null=False)
    fecha_pago = models.DateField('Fecha del pago',
                                auto_now_add=False,
                                blank=False,
                                null=True)
    estatus = models.CharField("Estatus del pago",
                            max_length=15,
                            unique=False,
                            blank=False,
                            help_text="Cadena de a lo mas 15 caracteres",
                            null=False)
    def __str__(self):
        return str(str(self.num_pago) + ": " + str(self.monto) + "/" + str(self.fecha_pago) + "(" + self.estatus + ")")

class Transacciones(models.Model):
    id = models.IntegerField("Id Transaccion",
                            primary_key=True,
                            unique=True,
                            null=False)
    cuenta_id = models.ForeignKey(Cuentas,
                                  db_column='cuenta_id',
                                  on_delete=models.DO_NOTHING,
                                  blank=False,
                                  null=False)
    fecha = models.DateTimeField('Fecha de la transaccion',
                                auto_now_add=False,
                                blank=False,
                                null=True)
    monto = models.DecimalField("Monto de la cuenta",
                            max_digits=15, 
                            decimal_places=2,
                            unique=False,
                            blank=False,
                            help_text="Decimal de a lo mas 15 digitos y 2 posiciones",
                            null=False)
    def __str__(self):
        return str(self.cuenta_id.id + ": " + str(self.monto) + "/" + str(self.fecha))